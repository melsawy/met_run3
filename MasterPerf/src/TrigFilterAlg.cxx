#include "TrigFilterAlg.h"

TrigFilterAlg::TrigFilterAlg(const std::string& name, ISvcLocator* pSvcLocator) :
  AthAnalysisAlgorithm(name, pSvcLocator)
{
  declareProperty("TrigDecisionTool", m_tdt);
}

TrigFilterAlg::~TrigFilterAlg() {}

StatusCode TrigFilterAlg::initialize()
{
  ATH_MSG_INFO("Initializing " << name() );
  ATH_CHECK( m_tdt.retrieve());
  ATH_MSG_INFO("... done");
  return StatusCode::SUCCESS;
}

StatusCode TrigFilterAlg::execute()
{
  setFilterPassed(m_triggers.size() == 0);
  for (const std::string& trig : m_triggers)
    if (m_tdt->isPassed(trig))
    {
      setFilterPassed(true);
      break;
    }
  return StatusCode::SUCCESS;
}

DECLARE_COMPONENT( TrigFilterAlg )