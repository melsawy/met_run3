====================================================
Template MET Trigger Performance Code for Release 22
====================================================

This repo is intended as an example of how to set up some basic (Athena-based) analysis code in release 22.
Long term we will probably want something a bit more complicated but this should be a sufficient launching point for most individual studies.
With respect to the previous version the script is now run entirely through ComponentAccumulators

Setup
-----

Prepare the directory structure and clone the git repository::

    mkdir build run
    git clone https://gitlab.cern.ch/jburr/masterperfstudies.git source

Setup the release and compile the code. For basic performance studies ``latest`` is sufficient but if doing longer-term work it's best to pick a stable release::

    cd build
    setupATLAS
    asetup Athena master latest
    cmake ../source
    make
    . ${Athena_PLATFORM}/setup.sh

On future sessions you will only need to set up the release and source the setup file.

Running the code
----------------

There is now only one running script ``run.py``.
This uses the metadata to work out if it is running on MC or data.

These should be run as::
    
    python -m MasterPerf.run /path/to/file1 /path/to/file2 etc

It is not able to work out if it is running over EB data, so in this case you must the flag ``--is-eb``.
To see all the available options run::

    python -m MasterPerf.run --help

Modifying the code
------------------

Make whatever modifications to the code that you like.
For now, it's probably best to copy this to your own git area and change it there.
As said in the introduction, longer term we will have a more official set of performance code maintained in the main trigger MET repository.

The main algorithm you will want to modify is the ``NTupleAlg.cxx``. This is where all the ``TTree`` output is assembled.
